const { Router } = require ( 'express' )
const router = Router ()

const express = require ( 'express' )

const api = express ()

api.set ( 'port', process.env.PORT || 3000 )

api.use ( express.urlencoded ( {extended: true } ) )

const request = require ('supertest')

const admin = require ( 'firebase-admin' )
var serviceAccount = require("../../../seguridadsl-ced72-firebase-adminsdk-yyk0x-b53de73b9a.json");

admin.initializeApp( {
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://seguridadsl-ced72.firebaseio.com/'
} )

const db = admin.database ()

api.get ( '/', ( req, res ) => {
  console.log ( 'Index is working!!' )
  res.sendfile ( './src/app/index.html')
} )

module.exports = api